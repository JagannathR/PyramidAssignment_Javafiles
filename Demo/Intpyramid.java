package demo;

import java.util.*;

class Demo {
public int count;
	/* A Function to accept the user Input */
	String accept() {
		String data;
	        int n;
      		System.out.println("Enter the Number");
      	 	Scanner scan = new Scanner(System.in);
		    n = scan.nextInt();
	        data = Integer.toString(n);
	        scan.close();
	        return data;
   	}


	/* A Function to print the data */
	void display(int[] value) {
		int k;
 
		System.out.println();		   
        	for(k = 0; k < value.length; k++)
	 	{			
		   	System.out.print(value[k] + "\t");
		}
	}


	/* A Function to get the input array and pass the elements whose sum has to be computed  */
	int[] compute(int[] b) 
	{ 
		int val;
		int j;	
   
		int[] a2 = new int[b.length - 1];                


	    for(j = b.length-1; j > 0; j--)
        {
           	val = recurcompute(b[j] + b[j-1]) ;	 
        	a2[j-1] = val;
        } 		
		display(a2);	
		return a2;
    }
	

	/* A Recursive Function to receive the elements and return their sum as a single digit */
	int recurcompute(int x)
	{
		int value = 0;     
       	if(x >= 9 )
       	{
       		value = recurcompute(x/10);
   		}
        value = value + x%10;
       	return value;
  	}
  
 }


class Intpyramid {
        
        /* The main function where the execution of the program begins*/
	public static void main(String args[]) {
			Demo d = new Demo();
	        String dev = d.accept();
   
	        int[] a1 = new int[dev.length()] ;
	        for(int i = 0; i < dev.length(); i++)
	        {      	    
	 	       a1[i] = dev.charAt(i) - '0';
	        } 

	        d.display(a1);

	        int n = a1.length;
	        while(n > 1)
	        {
	   	     a1 = d.compute(a1);  
    		}

  	}
 }
