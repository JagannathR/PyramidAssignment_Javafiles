package demo;

import java.util.*;

class Launch {	
	/* A Function to accept the Input from the user */
	String accept()  {
		System.out.println("Enter the String");
		Scanner scan = new Scanner(System.in);
		scan.useDelimiter("\n");
		String input = scan.next();
		input = input.toLowerCase();
		input = input.replaceAll("\\s", "");
		scan.close();
		return input;
	}
	

	/* A Function to get the input array and pass the elements whose sum has to be computed  */
	int[] compute(int[] b) 
	{ 
		int val;
		int j;	   
		int[] a2 = new int[b.length - 1];                

	    for(j = b.length-1; j > 0; j--)
        {
           	val = recurcompute(b[j] + b[j-1]) ;	 
        	a2[j-1] = val;
        } 		
		display(a2);	
		return a2;
    }


	/* A Recursive Function to receive the elements and return their sum as a single digit */
	int recurcompute(int x)
	{
		int value = 0;     
       	if(x > 9 )
       	{
       		value = recurcompute(x/10);
   		}
        value = value + x%10;
       	if(value > 9 )
       	{
       		value = recurcompute(value/10);
   		}
       	return value;
  	}

	
	/*  A Function to display the values of the string and their corresponding sums */
	void display(int[] print)
	{
		for(int k = 0; k < print.length; k++)
		{
			System.out.print(print[k] + "\t");
		}
		System.out.println();
	}
	
	
	/* A Function to get the stored values of the inputed string */
	int getValue(char c)
	{		
		HashMap<Character, Integer> hm1 = new HashMap<Character, Integer>();
		char ch1;
		int a;
		int i = 51;
		char x = '0';

		for(ch1 = 'a'; ch1 <= 'z'; ch1++)
		{
			hm1.put(ch1, i);			
			i++;			
		}
		for(int j = 0; j <=9; j++)
		{
			hm1.put(x, j);
			x++;
		}
		
		if(hm1.containsKey(c))
		{			
			a = (int) hm1.get(c);
		}
		else
		{
			a = -1;
		}
		return a;
	}

}

class Pyramid {
	public static void main(String args[])  {
		Launch d = new Launch();		
		String input = d.accept();	
		System.out.println(input);
		int[] a = new int[input.length()];
		int aj = 0;
		for(int j = 0 ; j < input.length(); j++)
		{
			System.out.print(input.charAt(j) + "\t");
			int m = d.getValue(input.charAt(j));
			if(m > 0)
			{
				a[aj] = m;
				aj++;
			}
		}
		System.out.println();
		d.display(a);

		int n = a.length;
		while(n > 0)
		{
			a = d.compute(a);  
			n -=1;				
		}

	}
}